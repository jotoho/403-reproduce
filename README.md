## 403-reproduce

The only purpose of this repository is to provide an example repo in reproducing [a bug in the GitLab Workflow extension](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues/457), that I reported.

You should just ignore this repo otherwise. :)
